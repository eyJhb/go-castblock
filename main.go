package main

import (
	"flag"

	"github.com/rs/zerolog"
	"gitlab.com/eyJhb/go-castblock/castblock"
)

var (
	flagNonFastTrackWait = flag.Int("nft-wait", 30, "How long to wait in seconds between checking if the Chromecast is in YouTube, and switch to FastTrack (FastTrack checks more often)")
	flagFastTrackWait    = flag.Int("ft-wait", 1, "How long to wait in seconds when is FastTrack mode, to check if an ad is playing")
	flagDiscoveryTimeout = flag.Int("discover-timeout", 3, "How long in seconds to wait when discovering Chromecast devices on the network")
	flagPurgeInterval    = flag.Int("purge-interval", 5*60, "How long in seconds between each purge of the devices. This is used to only discover new devices on the network every X seconds")

	flagDebug = flag.Bool("verbose", false, "Enable verbose")
)

func main() {
	flag.Parse()

	if *flagDebug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	conf := castblock.Config{
		NonFastTrackWait: *flagNonFastTrackWait,
		FastTrackWait:    *flagFastTrackWait,
		PurgeInterval:    *flagPurgeInterval,
		DiscoveryTimeout: *flagDiscoveryTimeout,
	}

	castblock.Run(conf)
}
