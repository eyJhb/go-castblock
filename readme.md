# go-castblock
This is a Go version of CastBlock found on GitHub: https://github.com/stephen304/castblock, which is a lot more optimised.

This version will NOT check for ads as fast as it can, but will instead use what has been called `fasttrack` to determine how often to check, if an ad is playing.
Besides this, there is also a dumb cache for the SponsorBlock API, which will cache the items for 20 minutes.

If device has been connected to, and the app connected to it is YouTube, then go-castblock will defaults to checking every second, if there is a video + ad that can be skipped.
If however there is nothing streaming to the Chromecast or the app that is being streamed is not YouTube, then it will use its `NonFastTrackWait`, which defaults to 30 seconds.

# Installing
Install by running

```
go install gitlab.com/eyJhb/go-castblock
```

# Features
- Configurable delays for checking ads
    - Will not use all your CPU + spam your network
- Build in dumb cache for SponsorBlock API

# Usage

```
go-castblock:
  -discover-timeout int
    	How long in seconds to wait when discovering Chromecast devices on the network (default 3)
  -ft-wait int
    	How long to wait in seconds when is FastTrack mode, to check if an ad is playing (default 1)
  -nft-wait int
    	How long to wait in seconds between checking if the Chromecast is in YouTube, and switch to FastTrack (FastTrack checks more often) (default 30)
  -purge-interval int
    	How long in seconds between each purge of the devices. This is used to only discover new devices on the network every X seconds (default 300)
  -verbose
    	Enable verbose
```

