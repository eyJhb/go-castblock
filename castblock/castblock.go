package castblock

import (
	"context"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/vishen/go-chromecast/dns"
)

type Config struct {
	// intervals
	PurgeInterval    int
	DiscoveryTimeout int

	// fasttrack intervals
	NonFastTrackWait int
	FastTrackWait    int
}

func Run(conf Config) {
	// init a sponsorblock cache
	sbc := &SponsorBlockCache{
		Entries: make(map[string]SponsorBlockCacheEntry),
	}

	// every 5 minutes we will destroy our devices list, and refresh it
	var lastdevicepurge time.Time
	var devices []*Device

	for {
		if time.Since(lastdevicepurge) >= time.Duration(conf.PurgeInterval)*time.Second {
			log.Info().Msg("purging devices and getting new")

			// loop over current devices and close connections
			for _, device := range devices {
				if err := device.Close(); err != nil {
					log.Error().Err(err).Str("name", device.device.DeviceName).Msg("failed to close device connection")
				}
			}
			// set to empty slice when all connections are closed
			devices = []*Device{}

			// setup context w/ deadline, and remember to cancel it afterwards to free resources
			d := time.Now().Add(time.Duration(conf.DiscoveryTimeout) * time.Second)
			ctx, cancelFunc := context.WithDeadline(context.Background(), d)
			tmpDevices, err := listDevices(ctx)
			cancelFunc()
			if err != nil {
				log.Error().Err(err).Msg("could not get devices")
				continue
			}

			// loop over tmpDevices and add our sponsorblock stuff
			for i := range tmpDevices {
				log.Info().Str("name", tmpDevices[i].device.DeviceName).Msg("found device")
				tmpDevices[i].sponsorCache = sbc
				tmpDevices[i].conf = conf

				// first run should be fast
				tmpDevices[i].fasttrack = true
			}

			// update devices, lastdevicepurge update
			devices = tmpDevices
			lastdevicepurge = time.Now()
		}

		// do the logic for each device
		for _, device := range devices {
			if err := device.DoLogic(); err != nil {
				log.Error().Err(err).Msg("failed to DoLogic")
			}
		}
	}
}

func listDevices(ctx context.Context) ([]*Device, error) {
	entries, err := dns.DiscoverCastDNSEntries(ctx, nil)
	if err != nil {
		return nil, err
	}

	var finalEntries []*Device
	for entry := range entries {
		finalEntries = append(finalEntries, &Device{device: entry})
	}

	return finalEntries, nil
}
