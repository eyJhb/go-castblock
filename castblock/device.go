package castblock

import (
	"errors"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/vishen/go-chromecast/application"
	"github.com/vishen/go-chromecast/cast"
	"github.com/vishen/go-chromecast/dns"
)

type Device struct {
	// sponsorblock cache that we use for them all!
	sponsorCache *SponsorBlockCache

	// connection to the "app" which is the device?
	app *application.Application

	// fasttrack - set if we are currently in youtube
	// which means that the DoLogic will run more often
	fasttrack bool

	// last update
	lastUpdated time.Time

	// underlying device!
	device dns.CastEntry

	// config that we have gotten from above
	conf Config
}

func (d *Device) DoLogic() error {
	log.Debug().Bool("fasttrack", d.fasttrack).Msg("fasttrack")
	if !d.fasttrack {
		time.Sleep(time.Duration(d.conf.NonFastTrackWait) * time.Second)
	} else {
		time.Sleep(time.Duration(d.conf.FastTrackWait) * time.Second)
	}

	// first ensure that we are in the youtube
	isWatching, err := d.IsWatchingYoutube()
	if err != nil {
		return err
	}
	log.Debug().Bool("isWatching", isWatching).Msg("isWatching")
	if !isWatching {
		// disable fasttrack, as we are no longer in youtube
		d.fasttrack = false
		return nil
	}
	// currently in youtube, then we enable fasttrack!
	d.fasttrack = true

	// ensure that we are currently playing something
	isPlaying, err := d.IsCurrentlyPlaying()
	log.Debug().Bool("isPlaying", isPlaying).Msg("isPlaying")
	if err != nil {
		return err
	}
	if !isPlaying {
		return nil
	}

	// get the video id we are currently watching
	videoId, err := d.GetCurrentVideoId()
	log.Debug().Str("videoId", videoId).Msg("videoId")
	if err != nil {
		return err
	}
	if videoId == "" {
		return errors.New("Could not get any video id??")
	}

	// check if there are any entries in videoblock?
	blocks, err := d.sponsorCache.GetSegments(videoId)
	if err != nil {
		if err == ErrNoSponsorBlock {
			log.Debug().Str("videoId", videoId).Msg("No SponsorBlock for this video")
			return nil
		}
		return err
	}

	// get current time
	_, media, err := d.getStatus()
	if err != nil {
		return err
	}
	if media == nil {
		return errors.New("Got a nil value for media, cannot continue")
	}

	currentTime := media.CurrentTime
	log.Debug().Float32("currentTime", currentTime).Msg("currentTime")

	for _, block := range blocks {
		blockStart, blockEnd := block.Segment[0], block.Segment[1]
		log.Debug().Float32("blockStart", blockStart).Float32("blockEnd", blockEnd).Msg("blockStart and blockEnd")

		// if this is true, then we want to skip it
		// i don't think we can be in two at a time, therefore return.
		if currentTime > blockStart && currentTime < blockEnd {
			log.Info().
				Str("videoId", videoId).
				Float32("CurrentTime", currentTime).
				Float32("blockStart", blockStart).
				Float32("blockEnd", blockEnd).
				Msg("skipping ad")

			return d.SeekTo(blockEnd)
		}
	}

	return nil
}

// SeekTo(value float32) error
func (d *Device) SeekTo(value float32) error {
	_, _, err := d.getStatus()
	if err != nil {
		return err
	}

	return d.app.SeekToTime(value)
}

// IsCurrentlyPlaying() (bool, error)
//   indicates if the stream is playing or paused
func (d *Device) IsCurrentlyPlaying() (bool, error) {
	_, media, err := d.getStatus()
	if err != nil {
		return false, err
	}

	if media == nil {
		return false, errors.New("IsCurrentlyPlaying: could no get media information")
	}

	if media.PlayerState == "PLAYING" {
		return true, nil
	}

	return false, nil
}

// IsWatchingYoutube() (bool, error)
func (d *Device) IsWatchingYoutube() (bool, error) {
	app, _, err := d.getStatus()
	if err != nil {
		return false, err
	}

	if app == nil {
		return false, errors.New("IsWatchingYoutube: could no get app information")
	}

	if app.DisplayName != "YouTube" {
		return false, nil
	}

	return true, nil
}

// GetCurrentVideoId() (string, error)
func (d *Device) GetCurrentVideoId() (string, error) {
	_, media, err := d.getStatus()
	if err != nil {
		return "", err
	}

	if media == nil {
		return "", errors.New("GetCurrentVideoId: could no get media information")
	}

	return media.Media.ContentId, nil
}

// getStatus will ensure we are connected and update the device
// any function that uses this, should ensure that the application/media
// returned is a non-nil value
func (d *Device) getStatus() (*cast.Application, *cast.Media, error) {
	if d.app == nil {
		if err := d.Connect(); err != nil {
			return nil, nil, err
		}
	}

	if time.Since(d.lastUpdated) > 500*time.Millisecond {
		if err := d.app.Update(); err != nil {
			return nil, nil, err
		}

		d.lastUpdated = time.Now()
	}

	app, media, _ := d.app.Status()
	return app, media, nil
}

// connect to the chromecast
func (d *Device) Connect() error {
	// ensure that we have already a closed connection
	if err := d.Close(); err != nil {
		return err
	}

	applicationOptions := []application.ApplicationOption{
		application.WithDebug(false),
		application.WithCacheDisabled(true),
	}

	app := application.NewApplication(applicationOptions...)
	if err := app.Start(d.device.AddrV4.String(), d.device.Port); err != nil {
		return err
	}

	d.app = app

	return nil
}

func (d *Device) Close() error {
	if d.app == nil {
		return nil
	}

	// close the app, do NOT stop the media playing
	err := d.app.Close(false)
	d.app = nil
	return err
}
