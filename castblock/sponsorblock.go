package castblock

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
)

var (
	ErrNoSponsorBlock = errors.New("No sponsorblock available for the requested video")
)

type SponsorBlockCache struct {
	Entries map[string]SponsorBlockCacheEntry

	sync.Mutex
}

type SponsorBlockCacheEntry struct {
	Blocks []SponsorBlock
	Added  time.Time
}

type SponsorBlock struct {
	Category string    `json:"category"`
	Segment  []float32 `json:"segment"`
	UUID     string    `json:"UUID"`
}

func (s *SponsorBlockCache) GetSegments(contentId string) ([]SponsorBlock, error) {
	s.Lock()
	defer s.Unlock()

	// do cleanup before anything
	s.cleanup()

	if v, ok := s.Entries[contentId]; ok {
		log.Debug().Str("videoId", contentId).Msg("found video in cache")
		return v.Blocks, nil
	}

	url := fmt.Sprintf("https://sponsor.ajay.app/api/skipSegments?videoID=%s", contentId)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 404 {
		return nil, ErrNoSponsorBlock
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("Did not get 200 OK status code from API")
	}

	var blocks []SponsorBlock
	if err := json.NewDecoder(resp.Body).Decode(&blocks); err != nil {
		return nil, err
	}

	// add it to our cache
	s.Entries[contentId] = SponsorBlockCacheEntry{
		Blocks: blocks,
		Added:  time.Now(),
	}

	log.Debug().Interface("segments", blocks).Str("videoID", contentId).Msg("found video using api")

	return blocks, nil
}

func (s *SponsorBlockCache) cleanup() {
	for key, val := range s.Entries {
		if time.Since(val.Added) > 20*time.Minute {
			delete(s.Entries, key)
		}
	}
}
