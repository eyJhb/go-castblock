{ pkgs ? import <nixpkgs> {} }:

let
  bin = pkgs.buildGoPackage rec {
    name = "go-castblock";
    version = "v0.0.1";
    goPackagePath = "gitlab.com/eyJhb/go-castblock";
    src = ./.;
    goDeps = ./deps.nix;
  };
in bin
# in pkgs.stdenv.mkDerivation rec {
#   name = "go-castblock";
#   src = ./.;
#   installPhase = ''
#     mkdir $out
#     cp ${bin}/bin/dsg-plus $out
#     cp -R ${src}/web $out
#   '';
# }

